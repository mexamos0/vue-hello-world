# Build Stage
FROM node:12.16-alpine3.10 AS buildstage

RUN apk update
RUN apk add --no-cache git
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# Final Stage
FROM node:12.16-alpine3.10
RUN npm install -g http-server
WORKDIR /app
COPY --from=buildstage /app/dist ./dist
EXPOSE 8080
CMD [ "http-server", "dist" ]